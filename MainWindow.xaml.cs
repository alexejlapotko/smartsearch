﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using SmartSearch.SearchTools;

namespace SmartSearch
{
    public static class LOG
    {
        public static System.Windows.Controls.TextBox textBox { get; set; }

        public static void Print(string text)
        {
            /*App.Current.Dispatcher.Invoke((Action)(() =>
            {
                textBox.Text = textBox.Text + text + "\r\n";
                textBox.ScrollToEnd();
            }));*/
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            App.indexedSearch.stopUpdate += () => update_box.Visibility = Visibility.Hidden;
            App.indexedSearch.startUpdate += () => update_box.Visibility = Visibility.Visible;

            requestHintOverlap.Text = "";
            requestHint.Text = "Поиск";

            this.DataContext = App.indexedSearch;
            requestBox.Focus();
        }

        private void requestBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string line = requestBox.Text.ToString(), hint;
            List<FileMatch> results = App.indexedSearch.FindMatches(line, out hint);

            if (sender == null)
            {
                line += hint + " ";
                requestBox.Text = line;
                requestBox.CaretIndex = line.Length;
                hint = "";
            }

            if (!line.Equals(""))
            {
                requestHintOverlap.Text = line;
                requestHint.Text = hint;
            }
            else
            {
                requestHintOverlap.Text = "";
                requestHint.Text = "Поиск";
            }

            if (results.Count > 0)
            {
                resultsCount.Text = results.Count.ToString();
                resultsDiscription.Text = "";
            }
            else
            {
                resultsCount.Text = "";
                if (!line.Equals("")) resultsDiscription.Text = "Поиск не дал результатов"; else resultsDiscription.Text = "";
            }

            if (hint.Equals(""))
                mathes_list.ItemsSource = results;
            else
            {
                mathes_list.ItemsSource = new List<object>();
                resultsDiscription.Text = "Нажмите клавишу 'Enter' для поиска по подсказке";
            }
        }

        private void mathes_list_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (mathes_list.SelectedItem != null)
            {
                SearchTools.FileMatch match = (SearchTools.FileMatch)mathes_list.SelectedItem;
                Process.Start(match.File.Path);
            }
        }

        private void settings_button_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settings = new SettingsWindow();
            settings.Owner = this;
            settings.ShowDialog();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                requestBox_TextChanged(null, null);
        }
    }
}
