﻿using SmartSearch.SearchTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SmartSearch
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();

            this.DataContext = App.indexedSearch;
            if (App.indexedSearch.Directories.Count == 1 && App.indexedSearch.Directories[0].Path.Equals("C:\\"))
            {
                all_dirs.IsChecked = true;
                dirs_grid.IsEnabled = false;
            }
            else
                choose_dirs.IsChecked = true;
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.RootFolder = System.Environment.SpecialFolder.DesktopDirectory;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                App.indexedSearch.Directories.AddDirectory(dialog.SelectedPath);
            }
        }

        private void remove_button_Click(object sender, RoutedEventArgs e)
        {
            if (dirs_list.SelectedIndex != -1)
            {
                App.indexedSearch.Directories.RemoveDirectory(App.indexedSearch.Directories[dirs_list.SelectedIndex]);

                if (App.indexedSearch.Directories.Count != 0)
                    dirs_list.SelectedIndex = 0;
            }
        }

        private void all_dirs_Checked(object sender, RoutedEventArgs e)
        {
            dirs_grid.IsEnabled = false;
            App.indexedSearch.Directories.ClearDirectories();
            App.indexedSearch.Directories.AddDirectory("C:\\");
        }

        private void choose_dirs_Checked(object sender, RoutedEventArgs e)
        {
            dirs_grid.IsEnabled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
