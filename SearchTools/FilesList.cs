﻿using SmartSearch.SearchTools.Parsing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SmartSearch.SearchTools
{
    public class ISFile
    {
        public string Path { get; set; }
        public List<WordMatch> Words { get; set; }
        public DateTime LastModifying { get; set; }
        public string Name
        {
            get
            {
                return System.IO.Path.GetFileName(Path);
            }
        }

        private static ImageSource ToImageSource(Icon icon)
        {
            ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }

        public ImageSource Icon
        {
            get
            {
                return ToImageSource(System.Drawing.Icon.ExtractAssociatedIcon(Path));
            }
        }

        public ISFile(string path)
        {
            Path = path;
            Words = new List<WordMatch>();

            LastModifying = new DateTime();
        }
    }

    public class FilesList : List<ISFile>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public event TaskQueueDelegate startUpdate;
        public event TaskQueueDelegate stopUpdate;

        private HashSet<String> paths;

        private TaskQueue scanFileQueue, clearFileQueue, updateFileQueue;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private IndexBase index;

        public FilesList(IndexBase _index)
        {
            index = _index;

            paths = new HashSet<string>();

            scanFileQueue = new TaskQueue(ScanFileTask);
            clearFileQueue = new TaskQueue(ClearFileTask);
            updateFileQueue = new TaskQueue(UpdateFileTask);

            scanFileQueue.startProcessingQueue += () => { if (startUpdate != null) startUpdate(); };
            scanFileQueue.stopProcessingQueue += () => { if (stopUpdate != null) stopUpdate(); };
        }

        public ISFile AddFile(string path)
        {
            if (paths.Contains(path)) return null;
            
            paths.Add(path);
            ISFile newFile = new ISFile(path);
            this.Add(newFile);

            scanFileQueue.AddTask(newFile);

            OnPropertyChanged("Count");

            return newFile;
        }

        public void RemoveFile(ISFile file)
        {
            this.Remove(file);
            paths.Remove(file.Path);

            updateFileQueue.RemoveTask(file);
            scanFileQueue.RemoveTask(file);
            clearFileQueue.AddTask(file);

            OnPropertyChanged("Count");
        }

        public void UpdateFile(ISFile file)
        {
            DateTime lastModifying = File.GetLastWriteTime(file.Path);
            if (lastModifying != file.LastModifying)
            {
                scanFileQueue.RemoveTask(file);
                updateFileQueue.AddTask(file);
            }
        }

        public bool RenameFile(ISFile file, string newPath)
        {
            if (!paths.Contains(newPath))
            {
                paths.Remove(file.Path);
                paths.Add(newPath);

                file.Path = newPath;

                updateFileQueue.AddTask(file);

                return true;
            } else
            {
                if (file != null) RemoveFile(file);
                UpdateFile(this.Find(f => f.Path.Equals(newPath)));
                return false;
            }
        }

        private void ScanFileTask(Object fileObj, CancellationToken cancelToken)
        {
            ISFile file = fileObj as ISFile;

            try
            {
                LOG.Print("Scanning file " + file.Path);

                file.LastModifying = File.GetLastWriteTime(file.Path);

                FileParser parser = FileTyper.GetFileParser(file, (word, position) =>
                    {
                        WordMatch match = index.AddWord(file, word, position);
                        file.Words.Add(match);
                    }, cancelToken);
                parser.Parse();

                LOG.Print("Scanned file " + file.Path);
            }
            catch (Exception e)
            {
                LOG.Print("!!Error when scanning file  " + file.Path + " : " + e.Message);
            }

        }

        private void ClearFileTask(Object fileObj, CancellationToken cancelToken)
        {
            ISFile file = fileObj as ISFile;

            LOG.Print("Clearing file " + file.Path);

            try
            {
                foreach (WordMatch wordMatch in file.Words)
                    index.RemoveWord(wordMatch);
                file.Words.Clear();
            }
            catch (Exception e)
            {
                LOG.Print("!!Error when clearing file  " + file.Path + " : " + e.Message);
            }

            LOG.Print("Cleared file " + file.Path);
        }

        private void UpdateFileTask(Object fileObj, CancellationToken cancelToken)
        {
            ClearFileTask(fileObj, cancelToken);
            ScanFileTask(fileObj, cancelToken);
        }
    }
}
