﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartSearch.SearchTools
{
    public class FileMatch
    {
        public ISFile File { get; set; }
        public List<WordMatch> Matches { get; set; }

        public FileMatch(ISFile file)
        {
            this.File = file;
            Matches = new List<WordMatch>();
        }
    }

    public class IndexedSearch : INotifyPropertyChanged
    {
        private static string DirsFileName
        {
            get { return ConfigurationManager.AppSettings["dirsFileName"]; }
        }

        private DirectoriesList dirs;
        private FilesList files;
        private IndexBase index;

        public event PropertyChangedEventHandler PropertyChanged;

        public event TaskQueueDelegate startUpdate;
        public event TaskQueueDelegate stopUpdate;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public DirectoriesList Directories
        {
            get
            {
                return dirs;
            }
        }

        public FilesList Files
        {
            get
            {
                return files;
            }
        }

        public IndexBase Words
        {
            get
            {
                return index;
            }
        }

        public IndexedSearch()
        {
            index = new IndexBase();
            files = new FilesList(index);
            dirs = new DirectoriesList(files);
            dirs.DirectoriesChanged += () =>
            {
                OnPropertyChanged("Directories");
                SaveToFile();
            };

            files.startUpdate += () => { if (startUpdate != null) startUpdate(); };
            files.stopUpdate += () => { if (stopUpdate != null) stopUpdate(); };

            LoadFromFile();
        }

        public List<FileMatch> FindMatches(string request, out string hint)
        {
            List<string> words = new List<string>();
            Regex rgx = new Regex("([А-Яа-яёЁ]{1,20}|[A-Za-z]{1,20}|[0-9]{1,20})");
            foreach (Match match in rgx.Matches(request))
            {
                string word = match.Value.ToLower();
                words.Add(word);
            }

            if (words.Count > 0)
            {
                string lastWord = words[words.Count - 1];
                string newWord = index.GetNearestWord(lastWord);
                words[words.Count - 1] = newWord;

                hint = newWord.Substring(lastWord.Length);
            }
            else hint = "";

            Dictionary<ISFile, FileMatch> fileMatches = new Dictionary<ISFile, FileMatch>();
            HashSet<ISFile> filesSet = null;

            foreach (string word in words)
            {
                List<WordMatch> matches = new List<WordMatch>(index.GetMatches(word));
                HashSet<ISFile> newFiles = new HashSet<ISFile>();

                foreach (WordMatch match in matches)
                    if (filesSet == null || filesSet.Contains(match.File))
                    {
                        newFiles.Add(match.File);
                        if (fileMatches.ContainsKey(match.File))
                        {
                            fileMatches[match.File].Matches.Add(match);
                        }
                        else
                        {
                            FileMatch fileMatch = new FileMatch(match.File);
                            fileMatches[match.File] = fileMatch;
                            fileMatch.Matches.Add(match);
                        }
                    }
                filesSet = newFiles;
            }

            List<FileMatch> res = new List<FileMatch>();
            foreach (var pair in fileMatches)
                if (filesSet.Contains(pair.Key)) res.Add(pair.Value);

            return res;
        }

        public void LoadFromFile()
        {
            List<string> dirsList;

            try
            {

                XmlSerializer formatter = new XmlSerializer(typeof(List<string>));
                FileStream aFile = new FileStream(DirsFileName, FileMode.Open);
                byte[] buffer = new byte[aFile.Length];
                aFile.Read(buffer, 0, (int)aFile.Length);
                MemoryStream stream = new MemoryStream(buffer);
                dirsList = (List<string>)formatter.Deserialize(stream);
                aFile.Close();
            }
            catch
            {
                dirsList = new List<string>();
            }

            foreach (string dir in dirsList)
            {
                if (Directory.Exists(dir))
                    dirs.AddDirectory(dir);
            }
            SaveToFile();
        }

        public void SaveToFile()
        {
            List<string> dirsList = new List<string>();
            foreach(ISDirectory dir in dirs)
                dirsList.Add(dir.Path);

            try
            {
                FileStream outFile = File.Create(DirsFileName);
                XmlSerializer formatter = new XmlSerializer(typeof(List<string>));
                formatter.Serialize(outFile, dirsList);
                outFile.Close();
            }
            catch { }
        }
    }
}
