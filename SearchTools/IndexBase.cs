﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools
{
    public enum MatchPlace : int { NAME = 1, CONTENT = 2 }

    public class MatchPosition
    {
        public MatchPlace Place { get; set; }
        public int PosFromStart { get; set; }

        public MatchPosition(MatchPlace place, int posFromStart)
        {
            Place = place;
            PosFromStart = posFromStart;
        }

        public override string ToString()
        {
            string place = "";
            if (Place == MatchPlace.NAME) place = "NAME";
            else if (Place == MatchPlace.CONTENT) place = "CONTENT";
            return "[" + place + "," + PosFromStart.ToString() + "]";
        }
    }

    public class WordMatch
    {
        public string Word { get; set; }
        public ISFile File { get; set; }
        public MatchPosition Position { get; set; }

        public WordMatch(string word, ISFile file, MatchPosition position)
        {
            Word = word;
            File = file;
            Position = position;
        }

        public override string ToString()
        {
            return Position.ToString() + " " + this.File.Name + " - " + this.Word;
        }
    }

    public class IndexBase : List<WordMatch>
    {
        private Dictionary<string, List<WordMatch>> matches;

        public IndexBase()
        {
            matches = new Dictionary<string,List<WordMatch>>();
        }

        public WordMatch AddWord(ISFile file, string word, MatchPosition position)
        {
            //LOG.Print("Add word '" + word + "' for " + file.Path + " on position " + position.ToString());

            if (matches.ContainsKey(word))
            {
                word = matches.SingleOrDefault(p => p.Key.Equals(word)).Key;

                WordMatch newMatch = new WordMatch(word, file, position);
                matches[word].Add(newMatch);
                this.Add(newMatch);

                return newMatch;
            }
            else
            {
                WordMatch newMatch = new WordMatch(word, file, position);
                matches[word] = new List<WordMatch>();
                matches[word].Add(newMatch);
                this.Add(newMatch);

                return newMatch;
            }
        }

        public void RemoveWord(WordMatch match)
        {
            //LOG.Print("Remove word '" + match.Word + "' for " + match.File.Path + " on position " + match.Position.ToString());

            List<WordMatch> wordMatches = matches[match.Word];
            wordMatches.Remove(match);
            if (wordMatches.Count == 0)
                matches.Remove(match.Word);

            this.Remove(match);
        }

        public List<WordMatch> GetMatches(string word)
        {
            if (matches.ContainsKey(word))
                return matches[word];
            else return new List<WordMatch>();
        }

        public List<string> GetWords()
        {
            List<string> list =  matches.Keys.ToList();
            list.Sort();
            return list;
        }

        public string GetNearestWord(string word)
        {
            if (matches.ContainsKey(word)) return word;

            string result = word;
            int resCount = 0;
            Dictionary<string, List<WordMatch>> tempMatches;
            try
            {
                tempMatches = new Dictionary<string, List<WordMatch>>(matches);
            }
            catch
            {
                return word;
            }
            foreach (var pair in tempMatches)
            {
                string compWord = pair.Key;
                int compCount = pair.Value.Count;
                if (compWord.Length > word.Length && word.Equals(compWord.Substring(0, word.Length)) && compCount > resCount)
                {
                    result = compWord;
                    resCount = compCount;
                }
            }

            return result;
        }
    }
}
