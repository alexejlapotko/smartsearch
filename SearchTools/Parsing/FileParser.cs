﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools.Parsing
{
    public delegate void FoundMatchDelegate(string word, MatchPosition position);

    public class FileParser
    {
        public ISFile File { get; set; }

        protected FoundMatchDelegate foundMatch;

        protected CancellationToken cancelToken;

        public FileParser(ISFile file, FoundMatchDelegate _foundMatch, CancellationToken _cancelToken)
        {
            this.File = file;
            foundMatch = _foundMatch;
            cancelToken = _cancelToken;
        }
        

        public virtual void Parse()
        {
            StringParser.Parse(Path.GetFileName(File.Path), (word, posFromStart) => foundMatch(word, new MatchPosition(MatchPlace.NAME, posFromStart)));
        }

        public static bool IsFileHaveType(ISFile file)
        {
            FileAttributes attr = System.IO.File.GetAttributes(file.Path);

            return ((attr & FileAttributes.Directory) != FileAttributes.Directory);
        }
    }
}
