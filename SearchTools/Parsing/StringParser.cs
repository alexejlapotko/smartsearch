﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools.Parsing
{
    public delegate void StringParserMatchDelegate(string word, int posFromStart);
    
    public static class StringParser
    {
        private static Regex rgx = new Regex("([А-Яа-яёЁ]{1,20}|[A-Za-z]{1,20}|[0-9]{1,20})");

        public static void Parse(string line, StringParserMatchDelegate foundMatch)
        {
            foreach (Match match in rgx.Matches(line))
            {
                string word = match.Value.ToLower();
                if (word[0] >= '0' && word[0] <= '9' && word.Length > 5) continue;
                foundMatch(word, match.Index);
            }
        }
    }
}
