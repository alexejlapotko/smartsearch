﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools.Parsing
{
    public class TextFileParser : FileParser
    {
        private static List<string> extensions = new List<string> { ".txt", ".pas" };

        public TextFileParser(ISFile file, FoundMatchDelegate _foundMatch, CancellationToken _cancelToken)
            : base(file, _foundMatch, _cancelToken) { }

        public override void Parse()
        {
            base.Parse();

            using (StreamReader reader = new StreamReader(this.File.Path, Encoding.Default))
	        {
	            string line;
                int pos = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    if (cancelToken.IsCancellationRequested) return;
                    StringParser.Parse(line, (word, posFromStart) => foundMatch(word, new MatchPosition(MatchPlace.CONTENT, pos + posFromStart)));
                    pos += line.Length;
                }
	        }
        }

        public static new bool IsFileHaveType(ISFile file)
        {
            if (FileParser.IsFileHaveType(file))
            {
                string ext = Path.GetExtension(file.Path);

                return extensions.Contains(ext);
            }
            else return false;
        }
    }
}
