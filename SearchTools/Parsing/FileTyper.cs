﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools.Parsing
{
    public static class FileTyper
    {
        public static FileParser GetFileParser(ISFile file, FoundMatchDelegate foundMatch, CancellationToken cancelToken)
        {
            if (TextFileParser.IsFileHaveType(file)) return new TextFileParser(file, foundMatch, cancelToken);
            if (WordDocFileParser.IsFileHaveType(file)) return new WordDocFileParser(file, foundMatch, cancelToken);
            if (HtmlFileParser.IsFileHaveType(file)) return new HtmlFileParser(file, foundMatch, cancelToken);

            return new FileParser(file, foundMatch, cancelToken);
        }
    }
}
