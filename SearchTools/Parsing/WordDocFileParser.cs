﻿using Spire.Doc;
using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools.Parsing
{
    public class WordDocFileParser : FileParser
    {
        private static List<string> extensions = new List<string> { ".doc", ".docx" };

        public WordDocFileParser(ISFile file, FoundMatchDelegate _foundMatch, CancellationToken _cancelToken)
            : base(file, _foundMatch, _cancelToken) { }

        public override void Parse()
        {
            base.Parse();

            Document document = new Document();
            document.LoadFromFile(File.Path);
            StringParser.Parse(document.GetText(), (word, posFromStart) => foundMatch(word, new MatchPosition(MatchPlace.CONTENT, posFromStart)));
        }

        public static new bool IsFileHaveType(ISFile file)
        {
            if (FileParser.IsFileHaveType(file))
            {
                string ext = Path.GetExtension(file.Path);

                if (!extensions.Contains(ext)) return false;

                try
                { 
                    Document document = new Document();
                    document.LoadFromFile(file.Path);
                }
                catch
                {
                    return false;
                }

                return true;
            }
            else return false;
        }
    }
}
