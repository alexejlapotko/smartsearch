﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SmartSearch.SearchTools.Parsing
{
    public class HtmlFileParser : FileParser
    {
        private static List<string> extensions = new List<string> { ".htm", ".html" };

        public HtmlFileParser(ISFile file, FoundMatchDelegate _foundMatch, CancellationToken _cancelToken)
            : base(file, _foundMatch, _cancelToken) { }

        private static Regex htmlTagRegex = new Regex(@"\<[\x20-\x7e]*\>");

        public override void Parse()
        {
            base.Parse();

            using (StreamReader reader = new StreamReader(this.File.Path, Encoding.UTF8))
            {
                string line;
                int pos = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    if (cancelToken.IsCancellationRequested) return;
                    foreach (var subline in htmlTagRegex.Split(line))
                    {
                        StringParser.Parse(subline, (word, posFromStart) => foundMatch(word, new MatchPosition(MatchPlace.CONTENT, pos + posFromStart)));
                    }
                    pos += line.Length;
                }
            }
        }

        public static new bool IsFileHaveType(ISFile file)
        {
            if (FileParser.IsFileHaveType(file))
            {
                string ext = Path.GetExtension(file.Path);

                return extensions.Contains(ext);
            }
            else return false;
        }
    }
}
