﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace SmartSearch.SearchTools
{
    public class ISDirectory
    {
        public string Path { get; set; }
        public List<ISFile> Files { get; set; }

        public ISDirectory(string path)
        {
            Path = path;
            Files = new List<ISFile>();
        }

        public bool Equals(ISDirectory obj)
        {
            if (obj == null) return false;
            return obj.Path == this.Path;
        }

        public ISFile FindFile(String filePath)
        {
            List<ISFile> dirFiles = Files;
            foreach (ISFile file in dirFiles)
                if (file.Path.Equals(filePath)) return file;
            
            return null;
        }
    }
    
    public delegate void DirectoriesChanged();

    public class DirectoriesList : List<ISDirectory>
    {
        private FilesList files;

        private TaskQueue scanDirQueue, removeDirQueue, checkDirQueue;
        private Dictionary<ISDirectory, FileSystemWatcher> watchers;

        public event DirectoriesChanged DirectoriesChanged;

        public DirectoriesList(FilesList _files)
        {
            files = _files;
            
            scanDirQueue = new TaskQueue(ScanDirTask);
            removeDirQueue = new TaskQueue(RemoveDirTask);
            checkDirQueue = new TaskQueue(CheckDirTask);

            watchers = new Dictionary<ISDirectory, FileSystemWatcher>();
        }

        public ISDirectory AddDirectory(string dirPath)
        {
            ISDirectory newDir = new ISDirectory(dirPath);

            if (!this.Contains(newDir))
            {
                this.Add(newDir);

                removeDirQueue.RemoveTask(newDir);

                FileSystemWatcher watcher = new FileSystemWatcher(newDir.Path);
                watchers[newDir] = watcher;
                watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                watcher.IncludeSubdirectories = true;

                watcher.Created += watcher_Created;
                watcher.Changed += watcher_Changed;
                watcher.Deleted += watcher_Deleted;
                watcher.Renamed += watcher_Renamed;
                
                watcher.EnableRaisingEvents = true;

                scanDirQueue.AddTask(newDir);

                DirectoriesChanged();
                
                return newDir;
            }
            else return null;
        }

        public void RemoveDirectory(ISDirectory dir)
        {
            if (this.Contains(dir))
            {
                watchers[dir].EnableRaisingEvents = false;
                watchers.Remove(dir);

                scanDirQueue.RemoveTask(dir);
                checkDirQueue.RemoveTask(dir);
                
                removeDirQueue.AddTask(dir);
                
                this.Remove(dir);

                DirectoriesChanged();
            }
        }

        public void ClearDirectories()
        {
            List<ISDirectory> dirs = new List<ISDirectory>(this);
            foreach (ISDirectory dir in dirs)
                this.RemoveDirectory(dir);
        }

        private void ScanDirTask(Object dirObj, CancellationToken cancelToken)
        {
            ISDirectory dir = dirObj as ISDirectory;

            LOG.Print("Scanning " + dir.Path);

            try
            {
                TreeScan(dir.Path, dir, cancelToken);
            }
            catch (Exception e)
            {
                LOG.Print("!!Error when scanning  " + dir.Path + " : " + e.Message + " : " + e.Message);
            }

            LOG.Print("Scanned " + dir.Path);
        }

        private void TreeScan(string path, ISDirectory rootDir, CancellationToken cancelToken)
        {     
            try
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    if (cancelToken.IsCancellationRequested) return;
                    App.Current.Dispatcher.Invoke((Action)(() => {
                        ISFile newFile = files.AddFile(file);
                        if (newFile != null) rootDir.Files.Add(newFile);
                    }));
                }
                foreach (string dirPath in Directory.GetDirectories(path))
                {
                    if (cancelToken.IsCancellationRequested) return;
                    TreeScan(dirPath, rootDir, cancelToken);
                }
            }
            catch (Exception e)
            {
            }
        }

        private void RemoveDirTask(Object dirObj, CancellationToken cancelToken)
        {
            ISDirectory dir = dirObj as ISDirectory;

            LOG.Print("Removing " + dir.Path);

            try
            {
                List<ISFile> dirFiles = new List<ISFile>(dir.Files);
                foreach (ISFile file in dirFiles)
                {
                    if (cancelToken.IsCancellationRequested) return;
                    files.RemoveFile(file);
                }
                dirFiles = new List<ISFile>(dir.Files);
                foreach (ISFile file in dirFiles)
                {
                    if (cancelToken.IsCancellationRequested) return;
                    files.RemoveFile(file);
                }
            }
            catch (Exception e)
            {
                LOG.Print("!!Error when removing  " + dir.Path + " : " + e.Message);
            }

            LOG.Print("Removed " + dir.Path);
        }

        private void CheckDirTask(Object dirObj, CancellationToken cancelToken)
        {
            ISDirectory dir = dirObj as ISDirectory;

            LOG.Print("Checking " + dir.Path);

            try
            {
                List<ISFile> dirFiles = new List<ISFile>(dir.Files);
                foreach (ISFile file in dirFiles)
                {
                    if (cancelToken.IsCancellationRequested) return;
                    if (!System.IO.File.Exists(file.Path))
                        App.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            dir.Files.Remove(file);
                            files.RemoveFile(file);
                        }));
                }
            }
            catch (Exception e)
            {
                LOG.Print("!!Error when checking  " + dir.Path + " : " + e.Message);
            }

            LOG.Print("Checked " + dir.Path);
        }

        private void watcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            ISDirectory dir = watchers.FirstOrDefault(x => x.Value == (FileSystemWatcher)sender).Key;

            try
            {
                FileAttributes attr = File.GetAttributes(e.FullPath);

                if ((attr & FileAttributes.Hidden) == FileAttributes.Hidden) return;

                if ((attr & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    LOG.Print("The file: " + e.FullPath + " has been added");
 
                    App.Current.Dispatcher.Invoke((Action)(() => {
                        ISFile newFile = files.AddFile(e.FullPath);
                        if (newFile != null) dir.Files.Add(newFile);
                    }));
                } else
                {
                    LOG.Print("The directory: " + e.FullPath + " has been added");

                    scanDirQueue.AddTask(dir);
                }
            }
            catch (Exception exp)
            {
                LOG.Print("!!Error when add file  " + e.FullPath + " : " + exp.Message);
            }
        }

        private void watcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            LOG.Print("Changes made to: " + e.FullPath);

            try 
            {
                ISDirectory dir = watchers.FirstOrDefault(x => x.Value == (FileSystemWatcher)sender).Key;

                FileAttributes attr = File.GetAttributes(e.FullPath);

                if ((attr & FileAttributes.Hidden) == FileAttributes.Hidden) return;

                if ((attr & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    ISFile file = dir.FindFile(e.FullPath);
                    files.UpdateFile(file);
                }
            } 
            catch (Exception exp)
            {
                LOG.Print("!!Error when change file  " + e.FullPath + " : " + exp.Message);
            }
        }

        private void watcher_Deleted(object sender, System.IO.FileSystemEventArgs e)
        {
            ISDirectory dir = watchers.FirstOrDefault(x => x.Value == (FileSystemWatcher)sender).Key;
            
            try
            {
                LOG.Print("The file: " + e.FullPath + " has been deleted");

                ISFile file = dir.FindFile(e.FullPath);
                if (file != null)
                {
                    dir.Files.Remove(file);
                    files.RemoveFile(file);
                }
                else
                    checkDirQueue.AddTask(dir);
            }
            catch (Exception exp)
            {
                LOG.Print("!!Error when deleting file  " + e.FullPath + " : " + exp.Message);
            }
        }

        private void watcher_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            ISDirectory dir = watchers.FirstOrDefault(x => x.Value == (FileSystemWatcher)sender).Key;

            try
            {
                FileAttributes attr = File.GetAttributes(e.FullPath);

                if ((attr & FileAttributes.Hidden) == FileAttributes.Hidden) return;

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    LOG.Print("Directory: " + e.OldFullPath + " renamed to " + e.FullPath);

                    checkDirQueue.AddTask(dir);
                    scanDirQueue.AddTask(dir);
                }
                else
                {
                    LOG.Print("File: " + e.OldFullPath + " renamed to " + e.FullPath);

                    ISFile file = dir.FindFile(e.OldFullPath);     
                    if (!files.RenameFile(file, e.FullPath) && file != null)
                    {
                        dir.Files.Remove(file);
                    }
                }   

            }
            catch (Exception exp)
            {
                LOG.Print("!!Error when rename to file  " + e.FullPath + " : " + exp.Message);
            }
        }
    }
}
