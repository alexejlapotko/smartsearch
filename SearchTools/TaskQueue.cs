﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartSearch.SearchTools
{
    delegate void ProcessDelegate(Object o, CancellationToken cancelToken);
    public delegate void TaskQueueDelegate();

    class TaskQueue
    {
        public ProcessDelegate Process { get; set; }

        private List<Object> queue;
        private Object curTask;
        private Task task;
        CancellationTokenSource cts;

        public event TaskQueueDelegate startProcessingQueue;
        public event TaskQueueDelegate stopProcessingQueue;

        public TaskQueue(ProcessDelegate process)
        {
            Process = process;

            queue = new List<Object>();
            curTask = null;
        }

        public void AddTask(Object o)
        {
            if (!queue.Contains(o) && curTask != o)
            {
                queue.Add(o);
                if (curTask == null) DoTasks();
            }
        }

        private async void DoTasks()
        {
            if (startProcessingQueue != null)
                startProcessingQueue();

            while (queue.Count > 0)
            {
                curTask = queue[0];
                queue.Remove(curTask);

                cts = new CancellationTokenSource();
                try
                {
                    await Task.Run(() => Process(curTask, cts.Token), cts.Token);
                }
                catch { }
            }
            curTask = null;
            task = null;

            if (stopProcessingQueue != null)
                stopProcessingQueue();
        }

        public void RemoveTask(Object o)
        {
            if (queue.Contains(o))
                queue.Remove(o);
            else if (curTask != null && curTask.Equals(o))
                cts.Cancel();
        }

        public int TaskCount
        {
            get
            {
                return queue.Count;
            }
        }
    }
}
